<?php
/**
 * @file
 * sportgarden_core.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function sportgarden_core_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function sportgarden_core_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function sportgarden_core_node_info() {
  $items = array(
    'training_instance' => array(
      'name' => t('Träningstillfälle'),
      'base' => 'node_content',
      'description' => t('Träningspass innehåller ett antal tillfällen, som matchar de tider och datum som ingår i träningspasset. Ett träningstillfälle kan till exempel vara Judo kyu, 2011-08-11, 19.00.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'training_session' => array(
      'name' => t('Träningspass'),
      'base' => 'node_content',
      'description' => t('Träningspass (som noder) används för att visa träningspass (som produkter) för besökare/användare, för att låta dem hitta de träningspass som passar just dem.'),
      'has_title' => '1',
      'title_label' => t('Namn'),
      'help' => '',
    ),
  );
  return $items;
}
