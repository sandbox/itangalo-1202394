<?php
/**
 * @file
 * sportgarden_core.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function sportgarden_core_default_rules_configuration() {
  $items = array();
  $items['rules_clone_and_add_a_week'] = entity_import('rules_config', '{ "rules_clone_and_add_a_week" : {
      "LABEL" : "Klona tr\\u00e4ningstillf\\u00e4lle",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rb_misc", "rules" ],
      "USES VARIABLES" : { "original_node" : { "label" : "Original node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "cloned_node" : { "new_instance" : "New instance" } },
            "DO" : [
              { "rb_misc_action_node_clone" : {
                  "USING" : { "node" : [ "original-node" ] },
                  "PROVIDE" : { "cloned_node" : { "new_instance" : "New instance" } }
                }
              },
              { "entity_save" : { "data" : [ "new-instance" ], "immediate" : 1 } }
            ],
            "LABEL" : "Kopiera tr\\u00e4ningstillf\\u00e4llet"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "new-instance" ],
                  "type" : { "value" : { "training_instance" : "training_instance" } }
                }
              }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "new-instance:field-instance-time-start" ],
                  "value" : {
                    "select" : "new-instance:field-instance-time-start",
                    "date_offset" : { "value" : 604800 }
                  }
                }
              },
              { "redirect" : { "url" : "node\\/[new-instance:nid]" } }
            ],
            "LABEL" : "\\u00c4ndra tr\\u00e4ningstid"
          }
        }
      ]
    }
  }');
  $items['rules_clone_instance'] = entity_import('rules_config', '{ "rules_clone_instance" : {
      "LABEL" : "Klona tr\\u00e4ningstillf\\u00e4lle",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "rb_misc" ],
      "ON" : [ "rb_misc_trigger_page_clone_instance_http_response" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "argument-entity-id--node-1" ],
            "type" : { "value" : { "training_instance" : "training_instance" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_clone_and_add_a_week" : { "original_node" : [ "argument-entity-id--node-1" ] } }
      ]
    }
  }');
  return $items;
}
