<?php
/**
 * @file
 * sportgarden_core.features.taxonomy.inc
 */

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function sportgarden_core_taxonomy_default_vocabularies() {
  return array(
    'martial_arts' => array(
      'name' => 'Kampsporter',
      'machine_name' => 'martial_arts',
      'description' => 'Denna vokabulär innehåller kampsporter, så som de är uppdelade efter gradering hos SportGarden',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '-10',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'training_facilities' => array(
      'name' => 'Träningslokaler',
      'machine_name' => 'training_facilities',
      'description' => 'Denna vokabulär innehåller de träningslokaler där SportGarden har pass.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-9',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
