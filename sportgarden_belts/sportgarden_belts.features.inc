<?php
/**
 * @file
 * sportgarden_belts.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function sportgarden_belts_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function sportgarden_belts_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function sportgarden_belts_node_info() {
  $items = array(
    'belt' => array(
      'name' => t('Grad'),
      'base' => 'node_content',
      'description' => t('Grader används för att ange hur många träningspass som krävs för att nå varje grad.'),
      'has_title' => '1',
      'title_label' => t('Gradnamn'),
      'help' => '',
    ),
    'belt_counter' => array(
      'name' => t('Graderingsräknare'),
      'base' => 'node_content',
      'description' => t('Graderingsräknare används för att hålla reda på hur många träningspass en person har i varje kampsport. De räknas upp automatiskt, men kan även justeras för hand.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
