<?php
/**
 * @file
 * sportgarden_belts.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function sportgarden_belts_default_rules_configuration() {
  $items = array();
  $items['rules_grant_new_belt'] = entity_import('rules_config', '{ "rules_grant_new_belt" : {
      "LABEL" : "Tilldela ny grad",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "rb_misc" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "belt_counter" : "belt_counter" } }
          }
        },
        { "rb_misc_action_views_result_count" : {
            "view" : "find_belt|default",
            "args" : "[node:field-counter-martial-art:tid]\\r\\n[node:field-counter-instances]",
            "minimum" : "1"
          }
        }
      ],
      "DO" : [
        { "rb_misc_action_views_load_node" : {
            "USING" : {
              "view" : "find_belt|default",
              "args" : "[node:field-counter-martial-art:tid]\\r\\n[node:field-counter-instances]"
            },
            "PROVIDE" : { "node" : { "new_belt" : "Ny grad" } }
          }
        },
        { "drupal_message" : { "message" : "Grattis! Du har nu f\\u00e5tt graden [new_belt:title]!" } },
        { "data_set" : { "data" : [ "node:field-counter-belt" ], "value" : [ "new-belt" ] } }
      ]
    }
  }');
  $items['rules_user_present_at_instance'] = entity_import('rules_config', '{ "rules_user_present_at_instance" : {
      "LABEL" : "Markera n\\u00e4rvaro vid tr\\u00e4ningstillf\\u00e4lle",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules", "rb_misc" ],
      "USES VARIABLES" : {
        "account" : { "label" : "Medlem", "type" : "user" },
        "instance" : { "label" : "Tr\\u00e4ningstillf\\u00e4lle", "type" : "node" }
      },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "entity_created" : { "created_counter" : "Created counter" } },
            "IF" : [
              { "NOT rb_misc_action_views_result_count" : {
                  "view" : "find_counter|default",
                  "args" : "[instance:nid]\\r\\n[account:uid]",
                  "minimum" : "1"
                }
              },
              { "node_is_of_type" : {
                  "node" : [ "instance" ],
                  "type" : { "value" : { "training_instance" : "training_instance" } }
                }
              }
            ],
            "DO" : [
              { "entity_create" : {
                  "USING" : {
                    "type" : "node",
                    "param_type" : "belt_counter",
                    "param_title" : "Graderingsr\\u00e4knare skapad [site:current-date]",
                    "param_author" : [ "site:current-user" ]
                  },
                  "PROVIDE" : { "entity_created" : { "created_counter" : "Created counter" } }
                }
              },
              { "data_set" : {
                  "data" : [ "created-counter:field-counter-user" ],
                  "value" : [ "account" ]
                }
              },
              { "data_set" : {
                  "data" : [ "created-counter:field-counter-martial-art" ],
                  "value" : [ "instance:field-instance-martial-art" ]
                }
              },
              { "entity_save" : { "data" : [ "created-counter" ], "immediate" : 1 } }
            ],
            "LABEL" : "S\\u00e4kerst\\u00e4ll att det finns en graderingsr\\u00e4knare"
          }
        },
        { "RULE" : {
            "PROVIDE" : { "node" : { "counter_loaded" : "Loaded counter" } },
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "instance" ],
                  "type" : { "value" : { "training_instance" : "training_instance" } }
                }
              }
            ],
            "DO" : [
              { "rb_misc_action_views_load_node" : {
                  "USING" : {
                    "view" : "find_counter|default",
                    "args" : "[instance:nid]\\r\\n[account:uid]"
                  },
                  "PROVIDE" : { "node" : { "counter_loaded" : "Loaded counter" } }
                }
              }
            ],
            "LABEL" : "H\\u00e4mta r\\u00e4knaren"
          }
        },
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "counter-loaded" ],
                  "type" : { "value" : { "belt_counter" : "belt_counter" } }
                }
              }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "counter-loaded:field-counter-instances" ],
                  "value" : {
                    "select" : "counter-loaded:field-counter-instances",
                    "num_offset" : { "value" : "1" }
                  }
                }
              }
            ],
            "LABEL" : "\\u00d6ka r\\u00e4knaren ett steg"
          }
        },
        { "RULE" : {
            "DO" : [
              { "data_set" : {
                  "data" : [ "account:field-user-instances-all" ],
                  "value" : {
                    "select" : "account:field-user-instances-all",
                    "num_offset" : { "value" : "1" }
                  }
                }
              }
            ],
            "LABEL" : "\\u00d6ka totalr\\u00e4knaren ett steg"
          }
        }
      ]
    }
  }');
  return $items;
}
