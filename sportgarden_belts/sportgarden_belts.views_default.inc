<?php
/**
 * @file
 * sportgarden_belts.views_default.inc
 */

/**
 * Implementation of hook_views_default_views().
 */
function sportgarden_belts_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'find_belt';
  $view->description = 'En vy som returnerar eventuella bälten som matchar given kampsport och antal träningstillfällen.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Hitta bälte';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Contextual filter: ID för kampsport */
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['id'] = 'field_belt_martial_art_tid';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['table'] = 'field_data_field_belt_martial_art';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['field'] = 'field_belt_martial_art_tid';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['ui_name'] = 'ID för kampsport';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_belt_martial_art_tid']['not'] = 0;
  /* Contextual filter: Antal träningstillfällen */
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['id'] = 'field_belt_number_of_trainings_value';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['table'] = 'field_data_field_belt_number_of_trainings';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['field'] = 'field_belt_number_of_trainings_value';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['ui_name'] = 'Antal träningstillfällen';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_belt_number_of_trainings_value']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'belt' => 'belt',
  );
  $export['find_belt'] = $view;

  $view = new view;
  $view->name = 'find_counter';
  $view->description = 'En vy som används av Rules för att hitta (eventuell) träningsräknare, utifrån en given person och ett träningstillfälle.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Hitta träningsräknare';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Kampsport */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['ui_name'] = 'Kampsport';
  $handler->display->display_options['relationships']['term_node_tid']['label'] = 'Kampsport';
  $handler->display->display_options['relationships']['term_node_tid']['required'] = 1;
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'martial_arts' => 'martial_arts',
    'training_facilities' => 0,
    'tags' => 0,
  );
  /* Relationship: Träningspass (produkt) */
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['id'] = 'reverse_field_session_martial_art_commerce_product';
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['field'] = 'reverse_field_session_martial_art_commerce_product';
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['relationship'] = 'term_node_tid';
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['ui_name'] = 'Träningspass (produkt)';
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['label'] = 'Träningspass (produkt)';
  $handler->display->display_options['relationships']['reverse_field_session_martial_art_commerce_product']['required'] = 1;
  /* Relationship: Träningstillfälle */
  $handler->display->display_options['relationships']['field_instance_session']['id'] = 'field_instance_session';
  $handler->display->display_options['relationships']['field_instance_session']['table'] = 'commerce_product';
  $handler->display->display_options['relationships']['field_instance_session']['field'] = 'field_instance_session';
  $handler->display->display_options['relationships']['field_instance_session']['relationship'] = 'reverse_field_session_martial_art_commerce_product';
  $handler->display->display_options['relationships']['field_instance_session']['ui_name'] = 'Träningstillfälle';
  $handler->display->display_options['relationships']['field_instance_session']['label'] = 'Träningstillfälle';
  $handler->display->display_options['relationships']['field_instance_session']['required'] = 1;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Contextual filter: Filtrera på ID för träningstillfälle */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'field_instance_session';
  $handler->display->display_options['arguments']['nid']['ui_name'] = 'Filtrera på ID för träningstillfälle';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['nid']['not'] = 0;
  /* Contextual filter: Filtrera på användar-ID för räknaren */
  $handler->display->display_options['arguments']['field_counter_user_uid']['id'] = 'field_counter_user_uid';
  $handler->display->display_options['arguments']['field_counter_user_uid']['table'] = 'field_data_field_counter_user';
  $handler->display->display_options['arguments']['field_counter_user_uid']['field'] = 'field_counter_user_uid';
  $handler->display->display_options['arguments']['field_counter_user_uid']['ui_name'] = 'Filtrera på användar-ID för räknaren';
  $handler->display->display_options['arguments']['field_counter_user_uid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_counter_user_uid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_counter_user_uid']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_counter_user_uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_counter_user_uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_counter_user_uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_counter_user_uid']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_counter_user_uid']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'belt_counter' => 'belt_counter',
  );
  $export['find_counter'] = $view;

  return $export;
}
